/*Copyright (C) 
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * 2021 - Chavez Aquiagual Victor Manuel
 */

#include "Entorno.h"

static bool validarRango ( size_t opc, size_t max )
{
	bool ban = true;
	
	if ( opc < 1 || opc > max )
	ban = false;

	return ban;
}

void Entorno_Gotoxy( int x, int y )
{
    printf( "%c[%d;%df", 0x1B, y, x );
}

void Entorno_Mensaje_Continuar( void ){
   
   char myChar = 0;

   Entorno_Gotoxy(5,41);
   printf( "Presione [ENTER] para continuar" );

   while ( myChar != '\n' && myChar != '\r' ){ 
      myChar = getchar(); 
   }
}

void Entorno_Mensaje_Next( void ){
   
   char myChar = 0;

   Entorno_Gotoxy(5,41);
   printf( "Presione [ENTER] para ir a la siguiente página" );

   while ( myChar != '\n' && myChar != '\r' ){ 
      myChar = getchar(); 
   }
}

void Entorno_Mensaje_Error( void ){
   
   char myChar = 0;

   Entorno_Gotoxy(5,41);
   printf( "Error, la opción no es válida! Presione [ENTER] para continuar" );

   while ( myChar != '\n' && myChar != '\r' ){ 
      myChar = getchar(); 
   }
}

void Entorno_Limpiar_Pantalla( void )
{
   system("clear");
   for ( int j = 0; j < HEIGHT; j++ ){
      for ( int i = 0; i < WIDTH; i++ ){
         Entorno_Gotoxy( i+1, j+1 );
         printf( BACKGROUND " " );
      }
   }
}

void Entorno_Limpiar_Pagina( void )
{
   for ( int j = 3; j < 37; j++ ){
      for ( int i = 1; i < 65; i++ ){
         Entorno_Gotoxy( i+1, j+1 );
         printf( BACKGROUND " " );
      }
   }
}

void Entorno_Limpiar_Terminal( void )
{
   Entorno_Gotoxy( 1, 41 );
   printf( "│ $                                                                                                                                │" );
}

void Entorno_Construir( void )
{
   int y = 1;
   Entorno_Limpiar_Pantalla();
   Entorno_Gotoxy( 1, y );
   printf( "┌───────────────────────┤Baal Player 7.0├────────────────────────┐┌─────────────────────────┤Pista Actual├─────────────────────────┐" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││ Nombre:                                                        │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││ Artista:                                                       │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││ Álbum:                                                         │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││ Comentario:                                                    │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││ Año:                                                           │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││ Género:                                                        │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││ Número de pista:                                               │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││ Calificación de la pista:                                      │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││ Calificación del álbum:                                        │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││                                                                │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││ [00:00]                    Detenido                    [--:--] │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││                                                                │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                │├───────────────────────────┤Playlist├───────────────────────────┤" ); y++; Entorno_Gotoxy( 1, y );
   for ( int i = y; i < 39; i++ ){
   printf( "│                                                                ││                                                                │" ); y++; Entorno_Gotoxy( 1, y );
   }
   printf( "├────┤ Página: 1 / 1           ├───────────────────────────────────────────────────────────────────────────────────────────────────┤" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                                                                                  │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│ $                                                                                                                                │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                                                                                  │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "└───────────────────────┤ Universidad Nacional Autónoma de México - Instituto Politécnico Nacional ├───────────────────────────────┘" ); y++; Entorno_Gotoxy( 1, y );

}

void Entorno_Pagina( int desde, int hasta )
{
   Entorno_Gotoxy( 1, 39 );
   printf( "├────┤ Página: %d / %d           ├───────────────────────────────────────────────────────────────────────────────────────────────────┤", desde, hasta );
}

void Entorno_Puntero_Terminal( void )
{
   Entorno_Gotoxy( 5, 40 );
   printf("\n");
   fprintf( stderr, "│ $ " );
}

void Entorno_Menu_Tutorial (void)
{
   int y = 2;

	Entorno_Construir();
   Entorno_Gotoxy(2,y);
      
	printf( "Tutorial para usar el reproductor" ); y=y+2; Entorno_Gotoxy(2,y);
   printf( "Introduce las siguientes letras en la terminal" ); y=y+2; Entorno_Gotoxy(2,y);

	printf( "\"P\" play/pause" ); y=y+1; Entorno_Gotoxy(2,y);
	printf( "\"B\" pista previa" ); y=y+1; Entorno_Gotoxy(2,y);
	printf( "\"F\" pista siguiente" ); y=y+1; Entorno_Gotoxy(2,y);
	printf( "\"S\" stop" ); y=y+2; Entorno_Gotoxy(2,y);
   
   printf( "Fin del tutorial" );

   Entorno_Mensaje_Continuar();
}

size_t Entorno_Menu_Principal (void)
{
   int y = 2;
	size_t opc;

	do{
      Entorno_Construir();
      Entorno_Gotoxy(2,y);
		printf( "Bienvenido a tu biblioteca de música" ); y=y+2; Entorno_Gotoxy(2,y);

		printf( "Menú de opciones" ); y=y+1; Entorno_Gotoxy(2,y);
		printf( "01 Explorar la biblioteca" ); y=y+1; Entorno_Gotoxy(2,y);
		printf( "02 Ver las listas de reproducción guardadas" ); y=y+1; Entorno_Gotoxy(2,y);
		printf( "03 Personalizar entorno" ); y=y+1; Entorno_Gotoxy(2,y);
		printf( "04 Opciones de depuración" ); y=y+1; Entorno_Gotoxy(2,y);
      printf( "05 Volver a ver el tutorial rápido" ); y=y+1; Entorno_Gotoxy(2,y);
		printf( "06 Salir" ); y=y+2; Entorno_Gotoxy(2,y);

      printf( "Por favor, seleccione una opcion" );

      Entorno_Gotoxy(5,41);
		scanf( "%ld", &opc );
      getchar();

      if ( validarRango( opc, 6 ) == false )
      Entorno_Mensaje_Error();

      y = 2;

	}while( validarRango( opc, 6 ) == false );
	
	return opc;
}

size_t Entorno_Menu_1 (void)
{
   int y = 2;
	size_t opc;

	do{
      Entorno_Construir();
      Entorno_Gotoxy(2,y);
		printf( "Explorar la biblioteca" ); y=y+2; Entorno_Gotoxy(2,y);

		printf( "01 Mostrar todos los artistas" ); y=y+1; Entorno_Gotoxy(2,y);
		printf( "02 Mostrar todas las canciones" ); y=y+1; Entorno_Gotoxy(2,y);
		printf( "03 Mostrar todos los álbumes" ); y=y+1; Entorno_Gotoxy(2,y);
		printf( "04 Mostrar todos los géneros" ); y=y+1; Entorno_Gotoxy(2,y);
      printf( "05 Mostrar todos los años" ); y=y+1; Entorno_Gotoxy(2,y);
      printf( "06 Mostrar por calificación de álbum" ); y=y+1; Entorno_Gotoxy(2,y);
      printf( "07 Mostrar por calificación de pistas" ); y=y+1; Entorno_Gotoxy(2,y);
		printf( "08 Regresar al menú anterior" ); y=y+2; Entorno_Gotoxy(2,y);

      printf( "Por favor, seleccione una opcion" );

      Entorno_Gotoxy(5,41);
		scanf( "%ld", &opc );
      getchar();

      if ( validarRango( opc, 8 ) == false )
      Entorno_Mensaje_Error();
      
      y = 2;

	}while( validarRango( opc, 8 ) == false );

	return opc;
}