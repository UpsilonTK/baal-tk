/*Copyright (C) 
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * 2021 - Chavez Aquiagual Victor Manuel
 */

#include "DLL.h"
#include "Entorno.h"

static Node* new_node( char* artista, char* album, char* comentario, size_t anhio, char* genero, size_t numCanciones, char** nombreCancion, size_t* calificacionCancion, size_t calificacionAlbum, size_t* duracion )
{
   Node* n = (Node*) malloc( sizeof( Node ) );
   
   if( n != NULL ){
      n->artista = artista;
	   n->album = album;
	   n->comentario = comentario;
	   n->anhio = anhio;
	   n->genero = genero;
	   n->numCanciones = numCanciones;
	   n->nombreCancion = nombreCancion;
	   n->calificacionCancion = calificacionCancion;
	   n->calificacionAlbum = calificacionAlbum;
      n->duracion = duracion;

      n->next = NULL;
      n->prev = NULL;
   }
   return n;
}

static void free_node( Node** this )
{
   assert( *this );

   free( (*this)->artista );
   (*this)->artista = NULL;
   free( (*this)->album );
   (*this)->album = NULL;
   free( (*this)->comentario );
   (*this)->comentario = NULL;
   free( (*this)->genero );
   (*this)->genero = NULL;
   free( (*this)->calificacionCancion );
   (*this)->calificacionCancion = NULL;
   free( (*this)->duracion );
   (*this)->duracion = NULL;
   for ( size_t i = 0; i < (*this)->numCanciones; i++ ){
      free( (*this)->nombreCancion[i] );
      (*this)->nombreCancion[i] = NULL;
   }
   free( (*this)->nombreCancion );
   (*this)->nombreCancion = NULL;

   free( *this );
   *this = NULL;
}

static char* uppercase( const char* word )
{
   char* buffer = (char*) malloc( strlen( word ) * sizeof( char ) );

   for( size_t i = 0; i < strlen( word ); i++ )
   buffer[i] = toupper( word[0] );

   return buffer;

   free ( buffer );
   buffer = NULL;
}

static char* estrellas( size_t calificacion )
{
   char* buffer = (char*) malloc( TAM * sizeof( char ) );

   switch ( calificacion ){

      case 0:
         strcpy( buffer, "☆☆☆☆☆" );
      break;

      case 1:
         strcpy( buffer, "★☆☆☆☆" );
      break;

      case 2:
         strcpy( buffer, "★★☆☆☆" );
      break;

      case 3:
         strcpy( buffer, "★★★☆☆" );
      break;

      case 4:
         strcpy( buffer, "★★★★☆" );
      break;

      case 5:
         strcpy( buffer, "★★★★★" );
      break;
   }

   return buffer;

   free ( buffer );
   buffer = NULL;
}

static char* ssm_2_hms( size_t ssm )
{
   char* buffer = (char*) malloc( TAM * sizeof( char ) );

   size_t h = ( ssm / 3600 );
   size_t m = ( ssm - ( 3600 * (h) ) ) / 60;
   size_t s = ( ssm - ( 3600 * (h) ) - ( 60 * (m) ) );

   if ( h == 0 ){
      sprintf( buffer, "%02ld:%02ld", m, s );
   } else{
      sprintf( buffer, "%02ld:%02ld:%02ld", h, m, s );
   }

   return buffer;

   free ( buffer );
   buffer = NULL;
}

static int comparador( const void* a, const void* b )
{
   return strcmp( uppercase( *(const char**)a ), uppercase( *(const char**)b ) );
}

/**
 * @brief Crea una lista doblemente enlazada
 *
 * @return Una referencia a la nueva lista
 * @post Una lista existente en el heap
 */
DLL* DLL_New()
{
   DLL* list = (DLL*) malloc( sizeof( DLL ) );
   if( list != NULL ){
      list->first = NULL;
      list->last = NULL;
      list->cursor = NULL;
      list->len = 0;
   }
   return list;
}

/**
 * @brief Destruye una lista.
 *
 * @param this Una lista.
 */
void DLL_Delete( DLL** this )
{
   assert( *this );

   while( (*this)->first != NULL ){
      DLL_Pop_back( *this );
   }

   free( *this );
   *this = NULL;
}

/**
 * @brief Devuelve una copia del TAD en el frente de la lista.
 *
 * @param this Una lista.
 *
 * @return La copia del TAD en el frente de la lista.
 *
 * @post Si la lista está vacía se dispara una aserción.
 */
Node* DLL_Front( DLL* this )
{
   assert( this->first != NULL );
   return this->first;
}

/**
 * @brief Devuelve una copia del TAD en la parte trasera de la lista.
 *
 * @param this Una lista.
 *
 * @return La copia del TAD en la parte trasera de la lista.
 *
 * @post Si la lista está vacía se dispara una aserción.
 */
Node* DLL_Back( DLL* this )
{
   assert( this->first != NULL );
   return this->last;
}

/**
 * @brief Inserta un elemento a la derecha del cursor.
 *
 * @param lista Una referencia a la lista de trabajo
 * @param item El elemento a insertar
 */
void DLL_Insert( DLL* this, Node* item )
{
   assert( item );

   if( this->first != NULL ){
      if( this->cursor->next == NULL ){
         DLL_Push_back( this, item );
      } else{
         item->prev = this->cursor; 
         DLL_Cursor_next( this ); 
         item->next = this->cursor; 
         DLL_Cursor_prev( this ); 
         this->cursor->next = item; 
         DLL_Cursor_next( this ); 
         DLL_Cursor_next( this ); 
         this->cursor->prev = item; 
         DLL_Cursor_prev( this ); 
      }
   } else{  
      this->first = this->last = this->cursor = item;
   }
   ++this->len;
}

/**
 * @brief Inserta un elemento en el frente de la lista.
 *
 * @param this Una lista.
 * @param item El elemento a insertar
 */
void DLL_Push_front( DLL* this, Node* item )
{
   assert( item );

   if( this->first != NULL ){
      this->first->prev = item;
      item->next = this->first;
      this->first = item;
   } else{
      this->first = this->last = this->cursor = item;
   }
   ++this->len;
}

/**
 * @brief Inserta un elemento en el fondo de la lista.
 *
 * @param this Una lista.
 * @param item El elemento a insertar
 */
void DLL_Push_back( DLL* this, Node* item )
{
   assert( item );

   if( this->first != NULL ){
      this->last->next = item;
      item->prev = this->last;
      this->last = item;
   } else{
      this->first = this->last = this->cursor = item;
   }
   ++this->len;
}

/**
 * @brief Elimina el elemento al frente de la lista.
 *
 * @param this Referencia a una lista.
 *
 * @post El cursor se mantiene en la posición en la que estaba cuando entró la
 * función.
 */
void DLL_Pop_front( DLL* this )
{
   assert( this->len );

   if( this->last != this->first ){
      Node* x = this->first->next;
      x->prev = NULL;
      free_node( &(this->first) );
      this->first = x;
      --this->len;
   } else{
      free_node( &(this->first) );
      this->first = this->last = this->cursor = NULL;
   }
}

/**
 * @brief Elimina el elemento al fondo de la lista.
 *
 * @param this Referencia a una lista.
 *
 * @post El cursor se mantiene en la posición en la que estaba cuando entró la
 * función.
 */
void DLL_Pop_back( DLL* this )
{
   assert( this->len );

   if( this->last != this->first ){
      Node* x = this->last->prev;
      x->next = NULL;
      free_node( &(this->first) );
      this->last = x;
      --this->len;
   } else{
      free_node( &(this->last) );
      this->first = this->last = this->cursor = NULL;
   }
}

/**
 * @brief Elimina un elemento.
 *
 * @param this Una lista.
 * @param pos Una posición válida. Éste se puede obtener de las diferentes funciones de movimiento del cursor y de las funciones de búsqueda.
 *
 * @return Una referencia al elemento a la derecha del elemento que se acaba de borrar.
 */
Node* DLL_Erase( DLL* this, Node* pos )
{
   assert( DLL_IsEmpty( this ) == false );

   Node* x = pos->next;

   return x;

}

/**
 * @brief Elimina el primer nodo que coincida con la llave.
 *
 * @param this Referencia a una lista.
 * @param key  Valor buscado
 *
 */
/*void DLL_Remove( DLL* this, size_t key )
{
   assert( DLL_IsEmpty( this ) == false );
   // ERR: no se puede borrar nada de una lista vacía

   Node* it = this->first;
   // |it| es la abreviación de "iterator", o  en español, "iterador"

   while( it != NULL ){

      if( it->datos == key ) DLL_Erase( this, it );

      it = it->next;
   }
}
*/

/**
 * @brief Elimina todos los elementos que den positivo a la función predicado.
 *
 * @param this Una lista.
 * @param cmp Función predicado. El elemento |y| es proporcionado por el argumento |key|, mientras que el valor |x| se obtiene de la lista. Por ejemplo, si la función quiere saber si el valor de la lista es menor que el valor del usuario (list_val < user_val), entonces la función podría ser: less_than( list_val, user_val), la cual se lee en conjunto: "Si el valor de la lista |list_val| es menor que el valor del usuario |user_val|, devuelve verdadero".
 * @param key El valor contra el que se está comparando.
 */
 /*
void DLL_Remove_if( DLL* this, bool (*cmp)( int x, int y ), int key )
{
   assert( DLL_IsEmpty( this ) == false );
   // ERR: no se puede eliminar nada de una lista vacía

   Node* it = this->first;
   // |it| es la abreviación de "iterator", o  en español, "iterador"

   while( it != NULL ){

      if( cmp( it->datos, key ) == true ) DLL_Erase( this, it );

      it = it->next;
   }
}
*/

/**
 * @brief Elimina todos los elementos de la lista sin eliminar la lista.
 *
 * @param this Referencia a una lista.
 */
void DLL_MakeEmpty( DLL* this )
{
   assert( this );

   while( this->first != NULL ){
      DLL_Pop_back( this );
   }
}

/**
 * @brief Coloca al cursor al inicio de la lista.
 *
 * @param this Una referencia a la lista de trabajo
 */
void DLL_Cursor_front( DLL* this )
{
   this->cursor = this->first;
}

/**
 * @brief Coloca al cursor al final de la lista.
 *
 * @param this Una referencia a la lista de trabajo
 */
void DLL_Cursor_back( DLL* this )
{
   this->cursor = this->last;
}

/**
 * @brief Mueve al cursor al siguiente elemento a la derecha.
 *
 * @param this Una lista.
 */
void DLL_Cursor_next( DLL* this )
{
   assert( this->cursor != NULL );

   Node* right = this->cursor->next;
   this->cursor = right;
}

/**
 * @brief Mueve al cursor al siguiente elemento a la izquierda.
 *
 * @param this Una lista.
 */
void DLL_Cursor_prev( DLL* this )
{
   assert( this->cursor != NULL );

   Node* left = this->cursor->prev;
   this->cursor = left;
}

/**
 * @brief Pone el cursor en una posición.
 *
 * @param this Una lista.
 * @param pos Una posición válida. Éste se puede obtener de las diferentes funciones de movimiento del cursor y de las funciones de búsqueda.
 */
void DLL_Cursor_set( DLL* this, Node* pos )
{
   assert( DLL_IsEmpty( this ) == false );

   this->cursor = pos;
}

/**
 * @brief Devuelve una copia del TAD en el cursor de la lista.
 *
 * @param this Una lista.
 *
 * @return La copia del TAD en el cursor de la lista.
 *
 * @post Si la lista está vacía se dispara una aserción.
 */
Node* DLL_Cursor_Get( DLL* this )
{
   assert( this->cursor );

   return this->cursor;
}

/**
 * @brief Indica si la lista está vacía.
 *
 * @param this Referencia a una lista.
 *
 * @return true si la lista está vacía; false en caso contrario.
 */
bool DLL_IsEmpty( DLL* this )
{
   return this->first == NULL;
}

/**
 * @brief Devuelve el número actual de elementos en la lista.
 *
 * @param this Una lista.
 *
 * @return Devuelve el número actual de elementos en la lista.
 */
size_t DLL_Len( DLL* this )
{

   assert( this );

   return this->len;
}

/**
 * @brief Recorre la lista y hace algo en cada uno de sus elementos
 *
 * @param lista Una referencia a la lista de trabajo
 * @param fn Esta función hace algo con el campo de datos 
 */
 /*
void DLL_Traverse( DLL* this, void (*fn)( int item ) )
{
	if( NULL == this ){ return; }

	Node* t = this->first;
   // ¡NO PODEMOS PERDER A FIRST!  
   
   while( t != NULL ){

		fn( t->datos );

		t = t->next;
   }
}
*/

/**
 * @brief Busca coincidencia con la cadena artista en la lista.
 *
 * @param this Una lista.
 * @param key El valor que se está buscando.
 *
 * @return Un apuntador al nodo donde se haya encontrado la primer coincidencia. Devuelve NULL en caso de no haber encontrado ninguna.
 */
Node* DLL_Find_artista( DLL* this, const char* key )
{
   assert( DLL_IsEmpty( this ) == false );
   // ERR: no se puede buscar nada en una lista vacía

   Node* it = this->first;
   // |it| es la abreviación de "iterator", o  en español, "iterador"

   while( it != NULL ){

      if( strcmp( uppercase( it->artista ), uppercase( key ) ) == 0 ) break;

      it = it->next;
   }

   return it;
   // ¡un único punto de salida!
}

/**
 * @brief Busca coincidencia con la cadena album en la lista.
 *
 * @param this Una lista.
 * @param key El valor que se está buscando.
 *
 * @return Un apuntador al nodo donde se haya encontrado la primer coincidencia. Devuelve NULL en caso de no haber encontrado ninguna.
 */
Node* DLL_Find_album( DLL* this, const char* key )
{
   assert( DLL_IsEmpty( this ) == false );
   // ERR: no se puede buscar nada en una lista vacía

   Node* it = this->first;
   // |it| es la abreviación de "iterator", o  en español, "iterador"

   while( it != NULL ){

      if( strcmp( it->album, key ) == 0 ) break;

      it = it->next;
   }

   return it;
   // ¡un único punto de salida!
}

/**
 * @brief Busca el valor año en la lista.
 *
 * @param this Una lista.
 * @param key El valor que se está buscando.
 *
 * @return Un apuntador al nodo donde se haya encontrado la primer coincidencia. Devuelve NULL en caso de no haber encontrado ninguna.
 */
Node* DLL_Find_anhio( DLL* this, size_t key )
{
   assert( DLL_IsEmpty( this ) == false );
   // ERR: no se puede buscar nada en una lista vacía

   Node* it = this->first;
   // |it| es la abreviación de "iterator", o  en español, "iterador"

   while( it != NULL ){

      if( it->anhio == key ) break;

      it = it->next;
   }

   return it;
   // ¡un único punto de salida!
}

/**
 * @brief Busca coincidencia con la cadena album en la lista.
 *
 * @param this Una lista.
 * @param key El valor que se está buscando.
 *
 * @return Un apuntador al nodo donde se haya encontrado la primer coincidencia. Devuelve NULL en caso de no haber encontrado ninguna.
 */
Node* DLL_Find_genero( DLL* this, const char* key )
{
   assert( DLL_IsEmpty( this ) == false );
   // ERR: no se puede buscar nada en una lista vacía

   Node* it = this->first;
   // |it| es la abreviación de "iterator", o  en español, "iterador"

   while( it != NULL ){

      if( strcmp( it->genero, key ) == 0 ) break;

      it = it->next;
   }

   return it;
   // ¡un único punto de salida!
}

/**
 * @brief Busca el valor numCanciones en la lista.
 *
 * @param this Una lista.
 * @param key El valor que se está buscando.
 *
 * @return Un apuntador al nodo donde se haya encontrado la primer coincidencia. Devuelve NULL en caso de no haber encontrado ninguna.
 */
Node* DLL_Find_numCanciones( DLL* this, size_t key )
{
   assert( DLL_IsEmpty( this ) == false );
   // ERR: no se puede buscar nada en una lista vacía

   Node* it = this->first;
   // |it| es la abreviación de "iterator", o  en español, "iterador"

   while( it != NULL ){

      if( it->numCanciones == key ) break;

      it = it->next;
   }

   return it;
   // ¡un único punto de salida!
}

/**
 * @brief Busca coincidencia con la cadena nombreCancion en la lista.
 *
 * @param this Una lista.
 * @param key El valor que se está buscando.
 * @param index El valor donde se encuentra en el arreglo.
 *
 * @return Un apuntador al nodo donde se haya encontrado la primer coincidencia. Devuelve NULL en caso de no haber encontrado ninguna.
 */
Node* DLL_Find_nombreCancion( DLL* this, const char* key, size_t* index )
{
   assert( DLL_IsEmpty( this ) == false );
   // ERR: no se puede buscar nada en una lista vacía

   Node* it = this->first;
   bool ban = false;
   // |it| es la abreviación de "iterator", o  en español, "iterador"

   while( it != NULL ){

      for ( size_t i = 0; i < it->numCanciones; i++ ){

         if( strcmp( it->nombreCancion[i], key ) == 0 ){
            ban = true;
            break;
         }
      }
      if( ban == true ){
         break;
      } else{
         it = it->next;
      }
   }

   return it;
   // ¡un único punto de salida!
}

/**
 * @brief Busca el valor de calificacion de la cancion la lista.
 *
 * @param this Una lista.
 * @param key El valor que se está buscando.
 * @param index El valor donde se encuentra en el arreglo.
 *
 * @return Un apuntador al nodo donde se haya encontrado la primer coincidencia. Devuelve NULL en caso de no haber encontrado ninguna.
 */
Node* DLL_Find_calificacionCancion( DLL* this, size_t key, size_t* index )
{
   assert( DLL_IsEmpty( this ) == false );
   // ERR: no se puede buscar nada en una lista vacía

   Node* it = this->first;
   // |it| es la abreviación de "iterator", o  en español, "iterador"

   while( it != NULL ){

      index = 0;
      for ( size_t i = 0; i < it->numCanciones; i++ ){

         if( it->calificacionCancion[i] == key ){
            break;
         } else{
            it = it->next;
            index++;
         }
      }
   }

   return it;
   // ¡un único punto de salida!
}

/**
 * @brief Busca un valor en la lista.
 *
 * @param this Una lista.
 * @param key El valor que se está buscando.
 *
 * @return Un apuntador al nodo donde se haya encontrado la primer coincidencia. Devuelve NULL en caso de no haber encontrado ninguna.
 */
Node* DLL_Find_calificacionAlbum( DLL* this, size_t key )
{
   assert( DLL_IsEmpty( this ) == false );
   // ERR: no se puede buscar nada en una lista vacía

   Node* it = this->first;
   // |it| es la abreviación de "iterator", o  en español, "iterador"

   while( it != NULL ){

      if( it->calificacionAlbum == key ) break;

      it = it->next;
   }

   return it;
   // ¡un único punto de salida!
}

/**
 * @brief Busca el valor de duración de la canción la lista.
 *
 * @param this Una lista.
 * @param key El valor que se está buscando.
 * @param index El valor donde se encuentra en el arreglo.
 *
 * @return Un apuntador al nodo donde se haya encontrado la primer coincidencia. Devuelve NULL en caso de no haber encontrado ninguna.
 */
Node* DLL_Find_duracion( DLL* this, size_t key, size_t* index )
{
   assert( DLL_IsEmpty( this ) == false );
   // ERR: no se puede buscar nada en una lista vacía

   Node* it = this->first;
   // |it| es la abreviación de "iterator", o  en español, "iterador"

   while( it != NULL ){

      index = 0;
      for ( size_t i = 0; i < it->numCanciones; i++ ){

         if( it->duracion[i] == key ){
            break;
         } else{
            it = it->next;
            index++;
         }
      }
   }

   return it;
   // ¡un único punto de salida!
}

/**
 * @brief Busca si algún valor en la lista cumple con la función predicado.
 *
 * @param this Una lista.
 * @param p_fn Función predicado. El elemento |y| es proporcionado por el argumento |key|, mientras que el valor |x| se obtiene de la lista. Por ejemplo, si la función quiere saber si el valor de la lista es menor que el valor del usuario (list_val < user_val), entonces la función podría ser: less_than( list_val, user_val), la cual se lee en conjunto: "Si el valor de la lista |list_val| es menor que el valor del usuario |user_val|, devuelve verdadero".
 * @param key El valor contra el que se está realizando la comparación.
 *
 * @return Un apuntador al nodo donde se haya encontrado la primer coincidencia. Devuelve NULL en caso de no haber encontrado ninguna.
 */
 /*
Node* DLL_Find_if( DLL* this, bool (*cmp)( int, int ), int key )
{
   assert( DLL_IsEmpty( this ) == false );
   // ERR: no se puede buscar nada en una lista vacía

   Node* it = this->first;
   // |it| es la abreviación de "iterator", o  en español, "iterador"

   while( it != NULL ){

      if( cmp( it->datos, key ) == true ) break;

      it = it->next;
   }

   return it;
}
*/

/**
 * @brief Imprime la estructura de la lista. Es para uso de depuración, no de uso general.
 *
 * @param this Una lista.
 */
void DLL_PrintStructure( DLL* this )
{

   if( DLL_IsEmpty( this ) ){
      fprintf( stderr, "\nLista vacía. Nada que mostrar.\n" );
   } else{

      Node* it = this->first;
      fprintf( stderr, "\nNil->" );

      while( it != NULL ){

         fprintf( stderr, "\nArtista: %s", it->artista );
         fprintf( stderr, "Álbum: %s", it->album );
         fprintf( stderr, "Calificación del álbum: %s", estrellas( it->calificacionAlbum ) );
         fprintf( stderr, "\nComentario: %s", it->comentario );
		   fprintf( stderr, "Año: %ld", it->anhio );
		   fprintf( stderr, "\nGénero: %s", it->genero );
         fprintf( stderr, "Número de pistas: %ld\n", it->numCanciones );
         for ( size_t i = 0; i < it->numCanciones; i++ ){
            fprintf( stderr, "\n" );
            fprintf( stderr, "Nombre de la canción %ld: %s", i+1, it->nombreCancion[i] );
            fprintf( stderr, "Calificación: %s", estrellas( it->calificacionCancion[i] ) );
            fprintf( stderr, "\nDuración: %s", ssm_2_hms( it->duracion[i] ) );
            fprintf( stderr, "\n" );
	      }
         fprintf( stderr, "\n***************************************************\n" );

         it = it->next;
      }
      fprintf( stderr, "Nil\n" );
   }
}

void DLL_Leer( DLL* this )
{
   Node* temp = new_node( NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 0, NULL );
   char* buffer = (char*) malloc( TAM * sizeof( char ) );

   printf( "\nIngrese el nombre del artista: " );
	fgets( buffer, TAM, stdin );
   temp->artista = (char*) malloc( strlen( buffer ) * sizeof( char ) );
   strcpy( temp->artista, buffer );

   printf( "Ingrese el nombre del álbum: " );
	fgets( buffer, TAM, stdin );
   temp->album = (char*) malloc( strlen( buffer ) * sizeof( char ) );
   strcpy( temp->album, buffer );

   printf( "Ingrese la calificación del álbum: " );
	scanf( "%ld", &(temp->calificacionAlbum) );
   fgets( buffer, TAM, stdin );

   printf( "Ingrese un comentario del álbum: " );
	fgets( buffer, TAM, stdin );
   temp->comentario = (char*) malloc( strlen( buffer ) * sizeof( char ) );
   strcpy( temp->comentario, buffer );

	printf( "Ingrese el año de lanzamiento: " );
	scanf( "%ld", &(temp->anhio) );
   fgets( buffer, TAM, stdin );

   printf( "Ingrese el género del álbum: " );
	fgets( buffer, TAM, stdin );
   temp->genero = (char*) malloc( strlen( buffer ) * sizeof( char ) );
   strcpy( temp->genero, buffer );
	
	printf( "Ingrese el número de pistas del álbum: " );
	scanf( "%ld", &(temp->numCanciones) );
   fgets( buffer, TAM, stdin );
	
   temp->nombreCancion = (char**) malloc( temp->numCanciones * sizeof( char* ) );
   temp->calificacionCancion = (size_t*) malloc( temp->numCanciones * sizeof( size_t ) );
   temp->duracion = (size_t*) malloc( temp->numCanciones * sizeof( size_t ) );

	for ( size_t i = 0; i < temp->numCanciones; i++ ){
		printf( "\nIngrese el nombre de la canción %ld : ", i+1 );
		fgets( buffer, TAM, stdin );
      temp->nombreCancion[i] = (char*) malloc( strlen( buffer ) * sizeof( char ) );
      strcpy( temp->nombreCancion[i], buffer );

      printf( "Ingrese la calificación de la canción %ld : ", i+1 );
      scanf( "%ld", &(temp->calificacionCancion[i]) );
      fgets( buffer, TAM, stdin );

      printf( "Ingrese la duración en segundos de la canción %ld : ", i+1 );
      scanf( "%ld", &(temp->duracion[i]) );
      fgets( buffer, TAM, stdin );
	}

   DLL_Push_back( this, temp );

   free ( buffer );
   buffer = NULL;
}

void DLL_Guardar ( DLL* this )
{
   FILE* biblioteca;
   Node* it = this->first;

   biblioteca = fopen( "biblioteca.txt", "w+" );
	
	if ( it == NULL ){
      fclose( biblioteca );
   } else{
      while ( it != NULL ){
         fprintf( biblioteca, "%s", it->artista );
         fprintf( biblioteca, "%s", it->album );
         fprintf( biblioteca, "%ld", it->calificacionAlbum );
         fprintf( biblioteca, "%s", "\n" );
         fprintf( biblioteca, "%s", it->comentario );
		   fprintf( biblioteca, "%ld", it->anhio );
         fprintf( biblioteca, "%s", "\n" );
		   fprintf( biblioteca, "%s", it->genero );
         fprintf( biblioteca, "%ld", it->numCanciones );
         fprintf( biblioteca, "%s", "\n" );
         for ( size_t i = 0; i < it->numCanciones; i++ ){
            fprintf( biblioteca, "%s", "\n" );
            fprintf( biblioteca, "%s", it->nombreCancion[i] );
            fprintf( biblioteca, "%ld", it->calificacionCancion[i] );
            fprintf( biblioteca, "%s", "\n" );
            fprintf( biblioteca, "%ld", it->duracion[i] );
            fprintf( biblioteca, "%s", "\n" );
	      }
         fprintf( biblioteca, "%s", "\n" );
		   fprintf( biblioteca, "%s", "***************************************************" );
         fprintf( biblioteca, "%s", "\n" );
         fprintf( biblioteca, "%s", "\n" );
			
			it = it->next;
		}// Fin del while para guardar toda la informacion
		
		fprintf( biblioteca, "%s", "@**FIN**" );
		fclose( biblioteca );
	}
}

void DLL_Cargar ( DLL* this )
{
   FILE* biblioteca;
   char* buffer = (char*) malloc( TAM * sizeof( char ) );
   bool ban = true;

   biblioteca = fopen( "biblioteca.txt", "r+" );
   fseek( biblioteca, 0, SEEK_END );

   if( ftell( biblioteca ) == 0 ) //El archivo esta vacio
   {
      fclose( biblioteca );
   } else{
      rewind( biblioteca );
      while( ban == true ){
         fgets( buffer, TAM, biblioteca );
         if ( buffer[0] == '@' ){ //Llegamos al final del fichero
            fclose( biblioteca );
            ban = false;
         } else{
            Node* temp = new_node( NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 0, NULL );

            temp->artista = (char*) malloc( strlen( buffer ) * sizeof( char ) );
            strcpy( temp->artista, buffer );

            fgets( buffer, TAM, biblioteca );
            temp->album = (char*) malloc( strlen( buffer ) * sizeof( char ) );
            strcpy( temp->album, buffer );

            fscanf( biblioteca, "%ld", &(temp->calificacionAlbum) );
            fgets( buffer, TAM, biblioteca );

            fgets( buffer, TAM, biblioteca );
            temp->comentario = (char*) malloc( strlen( buffer ) * sizeof( char ) );
            strcpy( temp->comentario, buffer );
	
            fscanf( biblioteca, "%ld", &(temp->anhio) );
            fgets( buffer, TAM, biblioteca );
   
            fgets( buffer, TAM, biblioteca );
            temp->genero = (char*) malloc( strlen( buffer ) * sizeof( char ) );
            strcpy( temp->genero, buffer );
	
	         fscanf( biblioteca, "%ld", &(temp->numCanciones) );
            fgets( buffer, TAM, biblioteca );
   
            temp->nombreCancion = (char**) malloc( temp->numCanciones * sizeof( char* ) );
            temp->calificacionCancion = (size_t*) malloc( temp->numCanciones * sizeof( size_t ) );
            temp->duracion = (size_t*) malloc( temp->numCanciones * sizeof( size_t ) );

	         for ( size_t i = 0; i < temp->numCanciones; i++ ){
               fgets( buffer, TAM, biblioteca );
		         fgets( buffer, TAM, biblioteca );
               temp->nombreCancion[i] = (char*) malloc( strlen( buffer ) * sizeof( char ) );
               strcpy( temp->nombreCancion[i], buffer );

               fscanf( biblioteca, "%ld", &(temp->calificacionCancion[i]) );
               fgets( buffer, TAM, biblioteca );

               fscanf( biblioteca, "%ld", &(temp->duracion[i]) );
               fgets( buffer, TAM, biblioteca );
	         }

            fgets( buffer, TAM, biblioteca );
            fgets( buffer, TAM, biblioteca );
            fgets( buffer, TAM, biblioteca );

            DLL_Push_back( this, temp );
         }
      }
   }
   free ( buffer );
   buffer = NULL;
}

void DLL_Imprimir_artistas( DLL* this ){

   int y = 4; // coordenada "y" para imprimir en pantalla
   int k = 0; // para insertar las cadenas en el arreglo y ordenarlas

   Node* arr = new_node( NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 0, NULL ); // usaré el campo de canciones para más fácil
   arr->nombreCancion = (char**) malloc( this->len * sizeof( char* ) ); // donde guardamos las cadenas
   Node* it = this->first; // para recorrer la lista
   
   while( it != NULL ){
      arr->nombreCancion[k] = it->artista;
      it = it->next;
      k++;
   }

   qsort( arr->nombreCancion, this->len, sizeof(const char*), comparador); // Lo ordenamos alfabeticamente

   k = 0; // lo reiniciamos ahora para imprimir la lista

   Entorno_Gotoxy( 2, y );

   if( DLL_Len( this ) <= 34 ){ // si los elementos caben en la pagina
      for( int i = 0; i < DLL_Len( this ); i++ ){  
         printf( "%02d %s", k+1, arr->nombreCancion[k] ); y=y+1; Entorno_Gotoxy( 2, y );
         k++;
      }
   } else{ // si los elementos no caben en la pagina

      int totalPages = (int)(( DLL_Len( this ) / 34 ) + 0.5 ); //obtenemos el numero de paginas sin residuo
      int remainingItems = DLL_Len( this ); // obtener los items restantes

      for( int j = 0; j < totalPages; j++ ){
         Entorno_Limpiar_Pagina();
         Entorno_Limpiar_Terminal();
         y = 4;
         Entorno_Gotoxy( 2, y );
         for( int i = 0; i < 33; i++ ){
            printf( "%02d %s", k+1, arr->nombreCancion[k] ); y=y+1; Entorno_Gotoxy( 2, y );
            k++;
         }
         remainingItems = remainingItems - 33; // restamos los items recien imprimidos
         printf( "+%d remaining items", remainingItems ); y=y+1; Entorno_Gotoxy( 2, y );
         if( ( DLL_Len( this ) % 34 ) != 0 ){
            totalPages = totalPages + 1;
            Entorno_Pagina( j+1, totalPages );
            totalPages = totalPages - 1;
         }
         Entorno_Mensaje_Next();
      }

      if( ( DLL_Len( this ) % 34 ) != 0 ){ // para imprimir la ultima pagina
         Entorno_Limpiar_Pagina();
         Entorno_Limpiar_Terminal();
         y = 4;
         Entorno_Gotoxy( 2, y );
         for( int i = 0; i < remainingItems; i++ ){
            printf( "%02d %s", k+1, arr->nombreCancion[k] ); y=y+1; Entorno_Gotoxy( 2, y );
            k++;
         }
         Entorno_Pagina( totalPages + 1, totalPages + 1 );
      }
   }
   free_node( &arr );
}

void DLL_Imprimir_canciones( DLL* this ){

   int y = 4; // para imprimir en pantalla
   int n = 0; // para el numero de canciones de la lista
   Node* arr = new_node( NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 0, NULL ); // usaré el campo de canciones para más fácil
   Node* it = this->first; // para recorrer la lista
   
   while( it != NULL ){ // para obtener el numero de canciones total

      n = n + it->numCanciones;

      it = it->next;
   }

   arr->nombreCancion = (char**) malloc( n * sizeof( char* ) ); // donde guardaremos las cadenas

   it = this->first;

   int k = 0; // para insertar las cadenas en el arreglo

   while( it != NULL ){
      for( size_t i = 0; i < it->numCanciones; i++ ){
         arr->nombreCancion[k] = it->nombreCancion[i];
         k++;
      }
      it = it->next;
   }

   qsort( arr->nombreCancion, n, sizeof(const char*), comparador); // Lo ordenamos para que se vea bien

   k = 0; // lo reiniciamos ahora para imprimir la lista

   Entorno_Gotoxy( 2, y );

   if( n < 34 ){ // si los elementos caben en la pantalla
      for( int i = 0; i < n; i++ ){
         printf( "%02d %s", k+1, arr->nombreCancion[k] ); y=y+1; Entorno_Gotoxy( 2, y );
         k++;
      }
   } else{ // si los elementos no caben en la pagina

      int totalPages = n / 34; //obtenemos el numero de paginas sin residuo
      int residuo = n % 34; // obtenemos el residuo
      int remainingItems = n; // obtener los items restantes

      for( int j = 0; j < totalPages; j++ ){
         Entorno_Limpiar_Pagina();
         Entorno_Limpiar_Terminal();
         y = 4;
         Entorno_Gotoxy( 2, y );
         for( int i = 0; i < 33; i++ ){
            printf( "%02d %s", k+1, arr->nombreCancion[k] ); y=y+1; Entorno_Gotoxy( 2, y );
            k++;
         }
         remainingItems = remainingItems - 33; // restamos los items recien imprimidos
         printf( "+%d remaining items", remainingItems ); y=y+1; Entorno_Gotoxy( 2, y );
         if( residuo != 0 ){
            totalPages = totalPages + 1;
            Entorno_Pagina( j+1, totalPages );
            totalPages = totalPages - 1;
         }
         Entorno_Mensaje_Next();
      }

      if( residuo != 0 ){ // para imprimir la ultima pagina
         Entorno_Limpiar_Pagina();
         Entorno_Limpiar_Terminal();
         y = 4;
         Entorno_Gotoxy( 2, y );
         for( int i = 0; i < remainingItems; i++ ){
            printf( "%02d %s", k+1, arr->nombreCancion[k] ); y=y+1; Entorno_Gotoxy( 2, y );
            k++;
         }
         Entorno_Pagina( totalPages + 1, totalPages + 1 );
      }
   }
   free_node( &arr );
}

void DLL_Imprimir_albumes( DLL* this ){

   int y = 4; // para imprimir en pantalla
   int k = 0; // para insertar las cadenas en el arreglo
   Node* arr = new_node( NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 0, NULL ); // usaré el campo de canciones para más fácil
   arr->nombreCancion = (char**) malloc( this->len * sizeof( char* ) ); // donde guardamos las cadenas
   Node* it = this->first; // para recorrer la lista
   
   while( it != NULL ){

      arr->nombreCancion[k] = it->album;

      it = it->next;
      k++;
   }

   qsort( arr->nombreCancion, this->len, sizeof(const char*), comparador); // Lo ordenamos para que sea orden abc

   k = 0; // lo reiniciamos ahora para imprimir la lista

   Entorno_Gotoxy( 2, y );

   if( DLL_Len( this ) < 34 ){ // si los elementos caben en la pagina
      for( int i = 0; i < DLL_Len( this ); i++ ){
         printf( "%02d %s", k+1, arr->nombreCancion[k] ); y=y+1; Entorno_Gotoxy( 2, y );
         k++;
      }
   } else{ // si los elementos no caben en la pagina

      int totalPages = DLL_Len( this ) / 34; //obtenemos el numero de paginas sin residuo
      int residuo = DLL_Len( this ) % 34; // obtenemos el residuo
      int remainingItems = DLL_Len( this ); // obtener los items restantes

      for( int j = 0; j < totalPages; j++ ){
         Entorno_Limpiar_Pagina();
         Entorno_Limpiar_Terminal();
         y = 4;
         Entorno_Gotoxy( 2, y );
         for( int i = 0; i < 33; i++ ){
            printf( "%02d %s", k+1, arr->nombreCancion[k] ); y=y+1; Entorno_Gotoxy( 2, y );
            k++;
         }
         remainingItems = remainingItems - 33; // restamos los items recien imprimidos
         printf( "+%d remaining items", remainingItems ); y=y+1; Entorno_Gotoxy( 2, y );
         if( residuo != 0 ){
            totalPages = totalPages + 1;
            Entorno_Pagina( j+1, totalPages );
            totalPages = totalPages - 1;
         }
         Entorno_Mensaje_Next();
      }

      if( residuo != 0 ){ // para imprimir la ultima pagina
         Entorno_Limpiar_Pagina();
         Entorno_Limpiar_Terminal();
         y = 4;
         Entorno_Gotoxy( 2, y );
         for( int i = 0; i < remainingItems; i++ ){
            printf( "%02d %s", k+1, arr->nombreCancion[k] ); y=y+1; Entorno_Gotoxy( 2, y );
            k++;
         }
         Entorno_Pagina( totalPages + 1, totalPages + 1 );
      }
   }
   free_node( &arr );
}

void DLL_Imprimir_generos( DLL* this ){ // como estos se repiten, se omitirán los términos repetidos

   int y = 4; // para imprimir en pantalla
   int k = 0; // para insertar las cadenas en el arreglo
   Node* arr = new_node( NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, 0, NULL ); // usaré el campo de canciones para más fácil
   arr->nombreCancion = (char**) malloc( this->len * sizeof( char* ) ); // donde guardamos las cadenas
   Node* it = this->first; // para recorrer la lista
   
   while( it != NULL ){

      arr->nombreCancion[k] = it->genero;

      it = it->next;
      k++;
   }

   qsort( arr->nombreCancion, this->len, sizeof(const char*), comparador); // Lo ordenamos para que sea orden abc

   k = 0; // lo reiniciamos ahora para imprimir la lista

   Entorno_Gotoxy( 2, y );

   if( DLL_Len( this ) < 34 ){ // si los elementos caben en la pagina
      for( int i = 0; i < DLL_Len( this ); i++ ){
         printf( "%02d %s", k+1, arr->nombreCancion[k] ); y=y+1; Entorno_Gotoxy( 2, y );
         k++;
      }
   } else{ // si los elementos no caben en la pagina

      int totalPages = DLL_Len( this ) / 34; //obtenemos el numero de paginas sin residuo
      int residuo = DLL_Len( this ) % 34; // obtenemos el residuo
      int remainingItems = DLL_Len( this ); // obtener los items restantes

      for( int j = 0; j < totalPages; j++ ){
         Entorno_Limpiar_Pagina();
         Entorno_Limpiar_Terminal();
         y = 4;
         Entorno_Gotoxy( 2, y );
         for( int i = 0; i < 33; i++ ){
            printf( "%02d %s", k+1, arr->nombreCancion[k] ); y=y+1; Entorno_Gotoxy( 2, y );
            k++;
         }
         remainingItems = remainingItems - 33; // restamos los items recien imprimidos
         printf( "+%d remaining items", remainingItems ); y=y+1; Entorno_Gotoxy( 2, y );
         if( residuo != 0 ){
            totalPages = totalPages + 1;
            Entorno_Pagina( j+1, totalPages );
            totalPages = totalPages - 1;
         }
         Entorno_Mensaje_Next();
      }

      if( residuo != 0 ){ // para imprimir la ultima pagina
         Entorno_Limpiar_Pagina();
         Entorno_Limpiar_Terminal();
         y = 4;
         Entorno_Gotoxy( 2, y );
         for( int i = 0; i < remainingItems; i++ ){
            printf( "%02d %s", k+1, arr->nombreCancion[k] ); y=y+1; Entorno_Gotoxy( 2, y );
            k++;
         }
         Entorno_Pagina( totalPages + 1, totalPages + 1 );
      }
   }
   free_node( &arr );
}

void DLL_Reproducir( DLL* this )
{
   struct timeval lapse;
   bool expiration;
   bool condition;
   int opc = 0;
   int y = 2;
   
   Entorno_Gotoxy( 77,2 );
   printf( "%s", this->cursor->nombreCancion[0] ); y=y+1; Entorno_Gotoxy( 78,y );
   printf( "%s", this->cursor->artista ); y=y+1; Entorno_Gotoxy( 76,y );
   printf( "%s", this->cursor->album ); y=y+1; Entorno_Gotoxy( 81,y );
   printf( "%s", this->cursor->comentario ); y=y+1; Entorno_Gotoxy( 74,y );
   printf( "%ld", this->cursor->anhio ); y=y+1; Entorno_Gotoxy( 77,y );
   printf( "%s", this->cursor->genero ); y=y+1; Entorno_Gotoxy( 86,y );
   printf( "%d", 1 ); y=y+1; Entorno_Gotoxy( 95,y );
   printf( "%s", estrellas( this->cursor->calificacionCancion[0] ) ); y=y+1; Entorno_Gotoxy( 93,y );
   printf( "%s", estrellas( this->cursor->calificacionAlbum ) ); y=y+2; Entorno_Gotoxy( 93,y );
   printf( "%s", "Reproduciendo" ); Entorno_Gotoxy( 125,y );
   printf( "%s", ssm_2_hms( this->cursor->duracion[0] ) );
   
   fd_set rfds;
   FD_ZERO( &rfds );
   FD_SET( 0, &rfds );

   for ( int i = 0; i < this->cursor->duracion[0] ; i++ ){
      lapse.tv_sec = 1;
      lapse.tv_usec = 0;
      do{
         expiration = select( 1, &rfds, NULL, NULL, &lapse );
         if( expiration == 0 ){ // reset the entry
            FD_ZERO( &rfds );
            FD_SET( 0, &rfds );
            Entorno_Gotoxy( 70,y );
            printf( "%s", ssm_2_hms( i+1 ) );
            Entorno_Puntero_Terminal();
         } else{
            // do something and then put condition true
            scanf("%d", &opc);
            if( opc > 5 )
            condition = true;
         }
      }while ( expiration != 0 && errno == EINTR );
      if ( condition == true ) break;
   }
}