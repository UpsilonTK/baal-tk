
#include "DLL.h"
#include "Entorno.h"

//----------------------------------------------------------------------
//  Driver program
//----------------------------------------------------------------------
int main( void )
{
   Entorno_Construir(); // Construimos el entorno

   DLL* biblioteca = DLL_New();
   DLL_Cargar( biblioteca );

   int opc = 0;

   Entorno_Menu_Tutorial();

   while( opc != 6 ){
      opc = Entorno_Menu_Principal();
      if( opc == 1 ){
         while( opc != 8 ){
            opc = Entorno_Menu_1();

            if( opc == 1 ){
               Entorno_Construir();
               Entorno_Gotoxy(2,2);
               printf( "Todos los artistas" );
               DLL_Imprimir_artistas( biblioteca );
               Entorno_Mensaje_Continuar();
            } else if( opc == 2 ){
               Entorno_Construir();
               Entorno_Gotoxy(2,2);
               printf( "Todas las canciones" );
               DLL_Imprimir_canciones( biblioteca );
               Entorno_Mensaje_Continuar();
            } else if( opc == 3 ){
               Entorno_Construir();
               Entorno_Gotoxy(2,2);
               printf( "Todos los �lbumes" );
               DLL_Imprimir_albumes( biblioteca );
               Entorno_Mensaje_Continuar();
            } else if( opc == 4 ){
               Entorno_Construir();
               Entorno_Gotoxy(2,2);
               printf( "Todos los g�neros" );
               DLL_Imprimir_generos( biblioteca );
               Entorno_Mensaje_Continuar();
            } else if( opc == 5 ){
               // continuara
            }
         }
      }
   }
   //DLL_Leer( deq->deq );

   //DLL_Reproducir( biblioteca->deq );

   DLL_Guardar( biblioteca );
   DLL_Delete( &biblioteca );

   return 1;
}